//machineList id, company, name, comIp, x, y
var cPage = 1;
var panel = false;
var colors, colors_a;
var subMenuTr = false;
var status3, status4;
var circleWidth = contentWidth * 0.07;
var canvas;
var ctx;
var dvcName = [];

$(function() {
	BindEvt();
	//$("#comName").html(window.sessionStorage.getItem("company"));
	setEl();
	//drawLine();
	setToday();
	//drawLineChart("lineChart");
	
	evtBind();
	getToday();
	
	//getDvcTemper(26);
	//getReportBarData();
	
	//pieChart();
	drawChart("chart1", 1);
	drawChart("chart2", 2);
});

var xAxis1 = ["1호기","2호기","3호기","4호기","5호기","6호기","7호기","8호기","9호기","10호기","11호기","12호기","13호기","14호기","15호기","16호기"];
var xAxis2 = ["17호기","18호기","19호기","20호기","21호기","22호기","23호기","24호기","25호기","26호기","27호기","28호기","29호기","30호기","31호기","32호기"];
function drawChart(id,type){
	if(type==1){
		xAxis = xAxis1;
	}else{
		xAxis = xAxis2;
	}
	 $('#' + id).highcharts({
	        chart: {
	            type: 'bar',
	            backgroundColor : "rgba(0,0,0,0)",
	            height : getElSize(1900)
	        },
	        credits : false,
	        exporting :false,
	        title: {
	            text: null
	        },
	        xAxis: {
	            categories: xAxis,
	            labels: {
	                style: {
	                    fontSize:getElSize(30)
	                }
	            }
	        },
	        yAxis: {
	            min: 1,
	            max : 24,
	            title: {
	                text: null
	            },
	        labels: {
                style: {
                    fontSize:getElSize(30)
                }
            }
	        },
	        legend: {
	            reversed: true,
	            enabled : false
	        },
	        plotOptions: {
	            series: {
	                stacking: 'normal',
	                lineWidth :getElSize(500)
	            }
	        },
	        series: [{
	            name: '',
	            data: [2, 0, 6, 0,  6,  5,  4, 4, 0, 2, 10, 3, 2, 5, 7,1],
	           color : "red"
	        }, {
	            name: 'Jane',
	            data: [5, 13, 8, 7,  2, 15, 3, 8, 7, 12, 0, 8, 10, 17, 2,0],
	            color : "yellow"
	        },{
	            name: 'Joe',
	            data: [2, 3, 6,  2,  6, 0,  4, 10, 0, 2, 10, 3, 2, 2, 7,1],
	            color : "orange"
	        }, {
	            name: 'Joe',
	            data: [15, 8, 4, 15, 10, 4, 13, 2, 17, 8, 4, 10, 10,0, 8,22],
	            color : "green"
	        }]
	    });
};

function BindEvt(){
	$("#corver").click(function(){
		hideCorver();
		$("#admin_pwd_box").fadeOut(500);
	});
	$("#alarm_title").click(showAlarmList);
	$("#tool").click(showRepairList);
};

var alarmListOpen = false;
function showAlarmList(){
	if(alarmListOpen){
		$("#alarmBox").fadeOut(500);
	}else{
		$("#alarmBox").fadeIn(500);
	};
	alarmListOpen = !alarmListOpen;
};

var toolListOpen = false;
function showRepairList(){
	if(toolListOpen){
		$("#repairBox").fadeOut(500);
	}else{
		$("#repairBox").fadeIn(500);
	};
	toolListOpen = !toolListOpen;
};

function enableDrawIcon(){
	$(".init_machine_icon").draggable({
		start: function(){
		},
		stop : function(){
			var offset = $(this).offset();
            var x = offset.left - marginWidth;
            var y = offset.top - marginHeight;
            var id = this.id;
            id = id.substr(6);
            
           setInitCardPos(id, setElSize(x), setElSize(y));
		},
	});
};


var blocked_icon = [];
var init_box_top = 0,
	init_box_left = 0;
var init_icon_offset = [];


function getToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
	
	$("#today").html(year + ". " + month + ". " + day + "<br>" + hour + ":" + minute + ":" + second)
	
	setTimeout(getToday,1000);
}

var report_opRatio
function getDvcTemper(dvcId){
	var url = ctxPath + "/chart/getDvcTemper.do";
	var param = "dvcId=" + dvcId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			thermometer(data.temperMill, "thermometer");
			thermometer(data.temperLeft, "thermometer2");
			thermometer(data.temperRight, "thermometer3");
		}
	});
};

function setSlideMode(){
	if(auto_flag){
		alert("Mode : Manual");
		auto_flag = false;
	}else{
		alert("Mode : Auto");
		slideIdx = 6;
		auto_flag = true;
		autoSlide();
	};
	return false;
};

var slideOrder = [
//                  	["$('#cam21').click()", 10000],
//                  	["$('#cam21').click()", 10000],
                  	["$('.icon:nth(9)').click()", 5000],
                  	["flipCard_r()", 10000],
                  	["$('#menu_btn').click()", 5000],
                  	["$('#panel_table td img:nth(1)').click()", 3000],
                  	["$('#menu_btn').click()", 10000],
                  	["$('#panel_table td img:nth(0)').click()", 3000],
                  ];

//var slideOrder = [
//["$('#wrap3 .machine_cam').click()", 2000], //NHP 6300
//["$('body #cam3').last().click()", 2000],
//["$('#wrap23 .icon').click()", 2000], //DHF8000
//["flipCard_r()", 2000],
//["$('#menu_btn').click()", 2000],
//["$('#panel_table td img:nth(1)').click()", 2000],
//["$('#menu_btn').click()", 2000],
//["$('#panel_table td img:nth(0)').click()", 2000],
//];

var slideIdx = 0;
function autoSlide(){
	if(auto_flag) {
		setTimeout(function(){
			eval(slideOrder[slideIdx][0]);
			slideIdx++;
			if(slideIdx==slideOrder.length){
				$("#svg").html("");
//				$("#cards").html("");
				
				$("#diagram").remove();
				$("#diagramTd").append("<div id='diagram'></div>");
				
				ctx.clearRect(0,0,canvas.width, canvas.height);
				
				$(".machine_video").each(function(idx, data){
					data.pause();
					data.src = '';
					delete this;
					//data.children('source').prop('src', '');
					data.remove();
				});
				$("#cards").empty();
				
				setTimeout(function(){
					slideIdx = 0;
					clearInterval(border_interval);
					clearInterval(status_interval);
					card_bar_data_array = [];
					new_card_bar_data_array = [];
					machine_card = "";
					$("#svg").show();
					$("#canvas").css("opacity",1);
					getAllMachine();
					getReportBarData();
					drawLineChart("lineChart");
				}, 500)
			}else{
				autoSlide();
			};
			
		},slideOrder[slideIdx][1]);
	}
};

var opRatio = 0;
var auto_flag = false;
function getReportBarData(){
	 var url = ctxPath + "/chart/getReportBarData.do";
	 var sDate = $("#sDate").val();
	 var eDate = $("#eDate").val();
	 var param = "sDate=" + sDate + 
	 			"&eDate=" + eDate;
	 
	 report_opRatio = 0;
	 $.ajax({
		url : url,
		type : "post",
		data : param,
		dataType : "json",
		success  : function(data){
			var json = data.dataList;
			reportChartName = new Array();
			noconnBar = new Array();
			alarmBar = new Array();
			waitBar = new Array();
			inCycleBar = new Array();
			opTime = 0;
			incycleTime_avg = 0;
			alatmTime_avg = 0;
			opRatio = 0;
			$(json).each(function(idx, data){
				reportChartName.push(data.name);
				//if(data.noconnTime>0){
					noconnBar.push(0);
//				};
				if(data.alarmTime>0){
					alarmBar.push(Number(Number(Number(data.alarmTime)/60/60).toFixed(1)));
				};
				if(data.waitTime>0){
					waitBar.push(Number(Number(Number(data.waitTime)/60/60).toFixed(1)));
				};
				if(data.incycleTime>0){
					inCycleBar.push(Number(Number(Number(data.incycleTime)/60/60).toFixed(1)));
				};
				
				//opTime += Number(data.incycleTime)/60/60/10;
				opRatio += Number(data.opRatio)
				incycleTime_avg += Number(data.incycleTime);
				alatmTime_avg += Number(data.alarmTime);
			});
			
			drawReportColumnChart("columnChart");
			$("#incycleTime_avg").html(Math.floor(incycleTime_avg/60/60/json.length))
			$("#alarmTime_avg").html(Math.floor(alatmTime_avg/60/60/json.length))

			$("#diagram").circleDiagram({
				textSize: getElSize(70), // text color
				percent : Number(opRatio/json.length).toFixed(1) + "%",
				size: getElSize(400), // graph size
				borderWidth: getElSize(30), // border width
				bgFill: "#95a5a6", // background color
				frFill: "#1abc9c", // foreground color
				//font: "serif", // font
				textColor: 'black' // text color
			});
		}
	 });
};

var opTime = 0;
var alatmTime_avg = 0;
var incycleTime_avg = 0;
var noconnBar = [];
var alarmBar = [];
var waitBar = [];
var inCycleBar = [];
function addSeries(){
	reportBar.addSeries({
		color : "gray",
		data : noconnBar
	}, true);
	
	reportBar.addSeries({
		color : "red",
		data : alarmBar
	}, true);
	
	reportBar.addSeries({
		color : "yellow",
		data : waitBar
	}, true);
	
	reportBar.addSeries({
		color : "green",
		data : inCycleBar
	});
}
var reportChartName = [];
function drawLine(x1,y1, x2,y2, x3,y3, x4,y4){
	ctx.moveTo(x1,y1);
	ctx.lineTo(x2,y2);
	ctx.lineTo(x3,y3);
	ctx.lineTo(x4,y4);
	ctx.lineWidth = getElSize(10);
	ctx.strokeStyle = "#ffffff";
	ctx.stroke();
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
};

function setToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	
//	$("#sDate").val(year + "-" + month + "-" + addZero(String(date.getDate()-1)));
	$("#sDate, #eDate").val(year + "-" + month + "-" + day);
};

//originally from http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
function formatCurrency(n, c, d, t) {
    "use strict";

    var s, i, j;

    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d === undefined ? "." : d;
    t = t === undefined ? "," : t;

    s = n < 0 ? "-" : "";
    i = parseInt(n = Math.abs(+n || 0).toFixed(c), 10) + "";
    j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function thermometer(temper, div) {
	var animate = false;

    var $thermo = $("#" + div),
        $progress = $(".progress", $thermo),
        $goal = $(".goal", $thermo),
        percentageAmount;

    var goalAmount = parseFloat( $goal.text() ),
    progressAmount = Number(temper),
    percentageAmount =  Math.min( Math.round(progressAmount / goalAmount * 1000) / 10, 100); //make sure we have 1 decimal point

    //let's format the numbers and put them back in the DOM
    $goal.find(".amount").text(formatCurrency( goalAmount ) );
    $progress.find(".amount").text(progressAmount + "°C");


    //let's set the progress indicator
    $progress.find(".amount").hide();
    if (animate !== false) {
        $progress.animate({
            "height": percentageAmount + "%"
        }, 1200, function(){
            $(this).find(".amount").fadeIn(500);
        });
    }else{
        $progress.css({
            "height": percentageAmount + "%"
        });
        $progress.find(".amount").fadeIn(500);
    };
};

function drawLineChart(id){
	$('#' + id).highcharts({
		chart : {
			height : getElSize(300),
			backgroundColor : "rgba(0,0,0,0)",
			marginBottom : 0,
			marginLeft :0,
			marginRight:0
		},
		exporting : false,
		credits : false,
        title: {
            text: false,
        },
        subtitle: {
            text: false,
        },
        xAxis: {
        	lineWidth: 0,
        	minorGridLineWidth: 0,
        	minorTickLength: 0,
        	tickLength: 0,
        	lineColor: 'transparent',
        	labels : {
        		enabled : false
        	},
        	gridLineWidth: 0
        },
        yAxis: {
        	labels : {
        		enabled : false
        	},
        	gridLineWidth: 0,
            title: {
                text: false
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        plotOptions : {
        	series : {
        		dataLabels: {
                    enabled: true,
                    style : {
                    	fontSize : getElSize(30),
                    	color : "black",
                    	textShadow: 0
                    },
                    formatter: function () {
                        if (this.point.options.showLabel) {
                            return this.y;
                        }
                        return null;
                    }
                },
            	lineWidth : getElSize(10),
            	marker: {
                    lineWidth: getElSize(10),
                    lineColor: null // inherit from series
                }
            },
        },
        tooltip: {
            //valueSuffix: '°C'
        },
        legend: {
        	enabled : false
        },
        series: []
    });
	
	var chart = $("#lineChart").highcharts()
	var data = Number(Number(Math.random() * 10).toFixed(1));
	chart.addSeries({data:[data], name : "Data"});
	
	for(var i = 0; i < 9; i++){
		data = Number(Number(Math.random() * 10).toFixed(1));
	 	chart.series[0].addPoint(data);
	 	$("#lineChartLabel").html(data);
	};
	
	callback();
};

function callback(){
	var chart = $("#lineChart").highcharts();
	var series = chart.series[0];
    var points = series.points;
    var pLen = points.length;
    var i = 0;
    var lastIndex = pLen - 1;
    var minIndex = series.processedYData.indexOf(series.dataMin);
    var maxIndex = series.processedYData.indexOf(series.dataMax);

    points[minIndex].options.showLabel = true;
	points[maxIndex].options.showLabel = true;
	//  points[lastIndex].options.showLabel = true;
	series.isDirty = true;
	chart.redraw();
};

function drawReportColumnChart(id){
	Highcharts.createElement('link', {
		   href: '//fonts.googleapis.com/css?family=Unica+One',
		   rel: 'stylesheet',
		   type: 'text/css'
		}, null, document.getElementsByTagName('head')[0]);
	
	 $('#' + id).highcharts({
		 	chart : {
		 		height : getElSize(1100),
		 		type: 'column',
		 		backgroundColor : "white",
		 		 style: {
		 	         fontFamily: "'Unica One', sans-serif"
		 	      },
		 	},
	        title: {
	            text: false,
	        },
	        exporting : false,
	        credits : false,
	        subtitle: {
	            text: false,
	        },
	        xAxis: {
	            categories: reportChartName,
                labels : {
	            	style : {
	   	        	 color : "white",
	   	        	 fontSize :getElSize(50)
	   	           },
	            },
	        },
	        yAxis: {
	        	max : 24,
	        	step : 2,
	            title: {
	                text: false
	            },
	            labels : {
	            	style : {
	   	        	 color : "white",
	   	        	 fontSize :getElSize(30)
	   	           },
//	   	           formatter: function () {
//	   	        	   if(this.value%2==0) return this.value;
//	   	           }
	            },
	        },
	        tooltip: {
	            enabled :false
	        },
	        plotOptions: {
	            series: {
	                lineWidth: getElSize(15),
	                borderWidth: 0,
	            },
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                	formatter : function(){
	                		if(this.y!=0){
	                			return this.y;
	                		}else{
	                			return null;
	                		};
	                	},
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
	                    style: {
	                        fontSize : getElSize(30),
	                        textShadow: '0 0 3px white'
	                    }
	                }
	            },
	        },
	        legend: {
	        	enabled : false
	        },
	        series: []
	         
	    });
	 
	 Highcharts.setOptions(Highcharts.theme);
	 
	 reportBar = $("#" + id).highcharts();
	 
	 addSeries();
};
var reportBar;
var block = 1/6;

function drawRect(x,y,w,h){
//	var w = getElSize(400);
//	var h = getElSize(80);
//	var x = (contentWidth/2) - (w/2);
//	var y = $(".mainTable").height() - marginHeight;
	
	ctx.strokeStyle = "gray";
	ctx.lineWidth = 0.3;
	ctx.rect(x,y+$(".mainTable").height() - marginHeight,w,h);
	ctx.lineJoin = 'round';
	ctx.stroke();
	
	ctx.fillStyle = "black";
	ctx.font = getElSize(40) + "px Calibri";
	//ctx.fillText("Reception Desk",x+getElSize(70),y + getElSize(40));
	
	ctx.fillStyle = "white";
	ctx.font = getElSize(33) + "px Calibri";
	//ctx.fillText("Staff Lounge Storage",getElSize(3520), getElSize(1050) - marginHeight);
	
};

function getAllMachine(){
	var url = ctxPath + "/chart/getAllMachine.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.machineList;

			var l_x1,l_y1,l_y2,l_y2;
			var r_x1,r_y1,r_y2,r_y2;
			var t_x1,t_y1,t_x2,t_y2,t_x3,t_y3;

			var icon = "";
			var class_name = "";
			var status = "";
			$(json).each(function(idx, data){
				if(data.status==null || data.status.toLowerCase()=="no-connection"){
					status = "NOCONN.svg";
				}else if(data.status.toLowerCase()=="wait"){ 
					status = "WAIT.svg";
				}else if(data.status.toLowerCase()=="alarm"){
					status = "ALARM.svg";
				}else if(data.status.toLowerCase()=="in-cycle"){
					status = "IN-CYCLE.svg";
				}else{
				
					status = "incycle.svg";
				};
				
				if(data.display==1){
					class_name = "display";
				}else{
					class_name = "hide";
				};
				icon += "<div class='init_machine_icon " + class_name + "' id='circle" + data.id + "' style='left : " + (getElSize(data.x) + marginWidth) + "; top:" + (getElSize(data.y) + marginHeight) +"; width :" + getElSize(data.w) + "';>" +
							"<span class='machine_name' id='machine_name_" + data.id + "' style='color:black; font-size:" + getElSize(30) + "'>" + decodeURIComponent(data.name) + "</span>" +
//							"<div class='init_circle'>" + 
								//"<img src=http://factory911.com:8080/emo/machine_images/" + data.pic + ".jpg class='init_icon' id=img" + data.id + ">" +
								"<img src=" +  ctxPath + "/images/THK/"+ data.pic + "-" + status + "?dummy=" + new Date().getMilliseconds() + " class='init_icon' id=img" + data.id + " style='    transform: rotate(" + data.rotate + "deg);'>" +
//							"</div>" + 
						"</div>";
			});
			
			drawRect(getElSize(1100), getElSize(50), getElSize(1650),getElSize(200)); drawRect(getElSize(2850), getElSize(50), getElSize(850),getElSize(200));
			drawRect(getElSize(1100), getElSize(350), getElSize(850),getElSize(350)); drawRect(getElSize(2050), getElSize(350), getElSize(1650),getElSize(350));
			drawRect(getElSize(1100), getElSize(750), getElSize(2600),getElSize(300));
			drawRect(getElSize(1100), getElSize(1100), getElSize(2700),getElSize(200));
			drawRect(getElSize(1100), getElSize(1350), getElSize(2700),getElSize(300));

			$("#main_table").css({
				"display" : "inline"
			});
			
			$("#svg").html(icon);
			setEl();
			
			stateBorder();
			
			$(".init_icon").css({
				"width": "100%",
				//"margin-top" : getElSize(50),
			});
			
			$(".machine_name").each(function(idx, data){
				var svg = $(data).parent().children("img");
				$(data).css({
					"position" : "absolute",
					"left" : (svg.width()/2) - ($(data).width()/2),
					"top" : (svg.width()/3.4/2)/2,
					"z-index" : 2,
					"font-weight" : "bolder"
				});
			});
			
			$(".init_icon").each(function(idx, data){
				$(data).css({
					"left" : (circleWidth/2) - $(data).width()/2,
					"top" : (circleWidth/2) - $(data).height()/2,
				});
			});
			
			
			
			//setTimeout(resetIcon, 3000);
		}
	});
	
};


var displayMachine = new Array();

var lineWidth = getElSize(5);
var toggle = true;

var alarmList = new Array();
var machineList = new Array();
var cardMachine = new Array();

function reArrangeIcon(){
	$(cardMachine).each(function(idx, data){
		$("#circle" + data[0]).animate({
			"left" : getElSize(data[1]) + marginWidth,
			"top" : getElSize(data[2]) + marginHeight + getElSize(100)
		}, 1000, function(){
			$("#circle" + data[0] + ", #canvas").animate({
				"opacity" : 0
			});
		});
	});
	
	setTimeout(function(){
		$("#svg").hide();
		$("#cards").animate({
			"opacity" : 1
		});
		autoSlide();
		clearInterval(border_interval);
		stateBorder();
	}, 1500);
	
	getMachineStatus();
	
	//setInterval(getMachineStatus, 1000*5);
};


function setDiagram(id, ratio){
	$("#diagram" + id).circleDiagram({
		textSize: getElSize(50), // text color
		percent : Number(ratio) + "%",
		size: getElSize(170), // graph size
		borderWidth: getElSize(20), // border width
		bgFill: "#95a5a6", // background color
		frFill: "#1abc9c", // foreground color
		//font: "serif", // font
		textColor: 'black' // text color
	});
};

var machine_card = "";
var card_bar_data_array = [];
var new_card_bar_data_array = [];
var status_interval = null;


var today;

function getTimeData(id, options){
	var url = ctxPath + "/chart/getTimeData.do";
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = addZero(String(date.getDate()));
	var hour = date.getHours();
	
	if(hour>=20){
		day = addZero(String(new Date().getDate()+1));
	};
	
	
	var today = year + "-" + month + "-" + day;
	var param = "workDate=" + today + 
				"&dvcId=" + id;
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		data : param,
		success : function(data){
			var json = data.statusList;
			
			var color = "";
			
			var status = json[0].status;
			if(status=="IN-CYCLE"){
				color = "green"
			}else if(status=="WAIT"){
				color = "yellow";
			}else if(status=="ALARM"){
				color = "red";
			}else if(status=="NO-CONNECTION"){
				color = "gray";
			};
			
			options.series.push({
				data : [ {
					y : Number(20),
					segmentColor : color
				} ],
			});
			
			$(json).each(function(idx, data){
				if(data.status=="IN-CYCLE"){
					color = "green"
				}else if(data.status=="WAIT"){
					color = "yellow";
				}else if(data.status=="ALARM"){
					color = "red";
				}else if(data.status=="NO-CONNECTION"){
					color = "gray";
				};
				options.series[0].data.push({
					y : Number(20),
					segmentColor : color
				});
			});
			
			for(var i = 0; i < 144-json.length; i++){
				options.series[0].data.push({
					y : Number(20),
					segmentColor : "rgba(0,0,0,0)"
				});
			};
			
			status = new Highcharts.Chart(options);
		}
	});
};

var statusColor = [];
var labelsArray = [ 20, 21, 22, 23, 24, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		13, 14, 15, 16, 17, 18, 19, 20 ];

function flipCard_r() {
	$("#part2").append($("#mainTable2"));
	$("#back").animate({
		"width" : card_width,
		"height" : card_height,
		"top" : card_top,
		"left" : card_left,
		"background-color" : "white"
	}, function() {
		//$("#part2").append($("#mainTable2"));
		$("#back").remove();
		$(".wrap").css({
			//"transition" : "0.5s",
			"-webkit-transform" : "rotateY(0deg)",
		});
	});
};

function parsingAlarm(str){
	var alarm = JSON.parse(str);
	
	var alarmMsg = " ";
	$(alarm).each(function(idx, data){
		if(data.ALARMMSG!="NULL" && typeof(data.ALARMMSG)!="undefined") alarmMsg += data.ALARMCODE + " - " + data.ALARMMSG + "<br>";
	});
	
	$("#alarm").css({
		"height" : getElSize(200)
	})
	
	$("#alarm").html(alarmMsg);
};

var card_width;
var card_height;
var card_top;
var card_left;
var spd_feed_interval = null;
function flipCard() {
	var $wrap = $(this).parent().parent().parent().parent().parent().parent(); 
	var idx = this.id.substr(3);
	var img = "<img src=" + ctxPath + "/images/company/doosan.png style='height:20%'>";
	

	
	//spd_feed_interval = setInterval(function(){
		for(var i = 0; i < card_bar_data_array.length; i++){
			if(card_bar_data_array[i].get("id")==idx){
				$("#machine_name").html(card_bar_data_array[i].get("name"));
				$(".neon1").html(Number(card_bar_data_array[i].get("opRatio")));
				$(".neon2").html(Number(card_bar_data_array[i].get("motorEndUpMovement")));
				$(".neon3").html(Number(card_bar_data_array[i].get("motorUpDownMovement")));
			}
		};
	//},3000);
	
		
	pieChart(idx);
	drawBarChart("container", idx);
		
	$wrap.css({
		"-webkit-transform" : "rotateY(180deg)",
	});

	var div = document.createElement("div");
	div.setAttribute("id", "back");
	var top = $wrap.offset().top;
	var left = $wrap.offset().left;
	var width = $wrap.width();
	var height = $wrap.height();

	var width_padding = Number($wrap.css("padding-left").substr(0,
			$wrap.css("padding-left").lastIndexOf("px"))) * 2;
	var height_padding = Number($wrap.css("padding-top").substr(0,
			$wrap.css("padding-top").lastIndexOf("px")))
			+ Number($wrap.css("padding-bottom").substr(0,
					$wrap.css("padding-bottom").lastIndexOf("px")));

	card_width = width + width_padding;
	card_height = height + height_padding;
	card_top = $wrap.offset().top;
	card_left = $wrap.offset().left;

	div.style.cssText = "background-color:white; " + "z-index:9999999; "
			+ "position:absolute; " + "top : " + top + "; " + "left:" + left
			+ "; " + "width:" + (width + width_padding) + "; " + "height:"
			+ (height + height_padding) + ";" + "border-radius : "
			+ getElSize(30) + "px " + 
			"background-images:url(../images/shopLayOut/card_back.png";

	$("#mainTable2").css("opacity", 0);

	setTimeout(function() {
		$("body").prepend(div);
		$(div).click(flipCard_r);
		$(div).animate({
			"border-radius" : 0,
			"width" : window.innerWidth,
			"height" : window.innerHeight,
			"top" : 0,
			"left" : 0,
			"background-color" : "rgb(234,234,234)",
		}, 500, function() {
			$(div).append($("#mainTable2"));
			
			for(var i = 0; i < machineList.length; i++){
				if(machineList[i][0]=="wrap" + idx)idx = i;
			};
			var machine_name = machineList[idx][2];
			var img = "<img src=" + ctxPath + "/images/company/" + machineList[idx][1].toLowerCase() + ".png style='height:20%'>";

			$("#mainTable2").animate({
				"opacity" : 1
			}, 500);
			
			$("#machine_name").css({
				"margin-top" : $("#machine_name_td").height() / 2
								- $("#machine_name").height() / 2
								- $(".subTitle").height()
			});
		});
	}, 500)
};

function hideRemoveBtn(){
	if(!rm_btn) return;	
	//$(".removeIcon").hide(500);
};

var rm_btn = false;
function evtBind() {
	cPage = $("#panel_table td img:nth(0)").attr("id");
	//$("div").not($(".wrap")).click(hideRemoveBtn);
	
	$("#menu_btn").click(togglePanel);
	$("#corver").click(function() {
		//if (panel)togglePanel();
	});
	
	$("#panel_table td img,#panel_table td div ").click(slidePage);
	
	$("#main_logo").click(function(){
		location.href = ctxPath + "/chart/detailChart.do";
	});
	
	$("#time_table").click(function(){
		location.href = ctxPath + "/chart/chart_real.do";
	});
};

var upPage = new Array();
var downPage = new Array();
var cPage;

function slidePage(){
	$("#panel_table img, #panel_table div").css("border",0)
	$(this).css({
		"border" : getElSize(20) + "px solid rgb(33,128,250)"
	});
	
	var id = this.id;
	
	if(cPage==id){
		$("#menu_btn").click();
		return;
	};
	
	upPage.push(cPage);
	var page = cPage.substr(cPage.lastIndexOf("_")+1);
	
	$("#part" + page).animate({
		"top" : - originHeight
	});
	
	cPage = id;
	
	page = id.substr(cPage.lastIndexOf("_")+1);

	$("#part" + page).animate({
		"top" : 0
	});
	
	togglePanel();
};

function showCorver(){
	$("#corver").css({
		"z-index":4,
		"opacity":0.7
	});
};

function hideCorver(){
	$("#corver").css({
		"z-index":-1,
		"opacity":0
	});
};

function togglePanel() {
	var panelDist;
	var btnDist;

	if (panel) {
		panelDist = -(originWidth * 0.2) - getElSize(20) * 2;
		btnDist = getElSize(30);
		
		hideCorver();
	} else {
		panelDist = 0;
		btnDist = (originWidth * 0.2) + ($("#menu_btn").width() / 3.5)
				+ getElSize(20);
		
		showCorver();
	};

	panel = !panel;

	$("#panel").animate({
		"left" : panelDist
	});
	$("#menu_btn").animate({
		"left" : btnDist
	});
};


function drawBarChart(id, idx) {
	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};
	colors = Highcharts.getOptions().colors;
	colors = [ {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
	}, ]

	var height = window.innerHeight;

	var options = {
		chart : {
			type : 'coloredarea',
			backgroundColor : 'rgba(0,0,0,0)',
			height : getElSize(800),
			width : getElSize(2500),
			marginTop : -(height * 0.2),
		},
		credits : false,
		title : false,
		xAxis : {
			categories : [24, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0,
					0, 0, 4, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0,
					0, 7, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 10,
					0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 12, 0, 0, 0, 0, 0, 13, 0, 0,
					0, 0, 0, 14, 0, 0, 0, 0, 0, 15, 0, 0, 0, 0, 0, 16, 0, 0, 0,
					0, 0, 17, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0,
					0, 20, 0, 0, 0, 0, 0, 21, 0, 0, 0, 0, 0, 22, 0, 0, 0, 0, 0,
					23, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0 ],
			labels : {

				formatter : function() {
					var val = this.value
					if (val == 0) {
						val = "";
					};
					return val;
				},
				style : {
					color : "white",
					fontSize : getElSize(15),
					fontWeight : "bold"
				},
			}
		},
		yAxis : {
			labels : {
				enabled : false,
			},
			title : {
				text : false
			},
		},
		tooltip : {
			enabled : false
		},
		plotOptions : {
			line : {
				marker : {
					enabled : false
				}
			}
		},
		legend : {
			enabled : false
		},
		series : []
	};

	$("#" + id).highcharts(options);

	var status = $("#container").highcharts();
	var options = status.options;

	options.series = [];
	options.title = null;
	options.exporting = false;

	//getTimeData(idx, options)
	
	options.series.push({
		data : [ {
			y : Number(20),
			segmentColor : "gray"
		} ],
	});
	
	var color = "red"
	for(var i = 0; i < 100; i++){
		var n = Math.random()*10;
		if(n<=4){
			color = "green";
		}else if(n<=7){
			color = "yellow";
		}else if(n<=10){
			color = "red";
		}
//		if(data.status=="IN-CYCLE"){
//			color = "green"
//		}else if(data.status=="WAIT"){
//			color = "yellow";
//		}else if(data.status=="ALARM"){
//			color = "red";
//		}else if(data.status=="NO-CONNECTION"){
//			color = "gray";
//		};
		options.series[0].data.push({
			y : Number(20),
			segmentColor : color
		});
	};
	
	for(var i = 0; i < 144-100; i++){
		options.series[0].data.push({
			y : Number(20),
			segmentColor : "rgba(0,0,0,0)"
		});
	};
	
	status = new Highcharts.Chart(options);
};

function timeConverter(UNIX_timestamp) {
	var a = new Date(UNIX_timestamp * 1000);
	var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
			'Sep', 'Oct', 'Nov', 'Dec' ];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':'
			+ sec;
	return time;
}

function pieChart(idx) {
	Highcharts.setOptions({
		// green yellow red gray
		colors : [ 'rgb(100,238,92 )', 'rgb(250,210,80 )', 'rgb(231,71,79 )',
				'#8C9089' ]
	});

	$('#pie')
			.highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							type : 'pie',
							backgroundColor : "rgba(0,0,0,0)"
						},
						credits : false,
						exporting : false,
						title : {
							text : false
						},
						legend : {
							enabled : false
						},
						tooltip : {
							pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions : {
							pie : {
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : true,
									format : '<b>{point.name}</b>: {point.percentage:.1f} %',
									style : {
										color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
												|| 'white',
										fontSize : getElSize(30),
										textShadow: 0
									}
								},
								showInLegend : true
							}
						},
						series : [ {
							name : "Brands",
							colorByPoint : true,
							data : [ {
								name : "In-Cycle",
								y : 13
							}, {
								name : "Wait",
								y : 2
							}, {
								name : "Alarm",
								y : 1
							}, {
								name : "No-Connection",
								y : 0
							}]
						} ]
					});
	
	var chart = $("#pie").highcharts();
	
	var incycle = 0;
	var wait = 0;
	var alarm = 0;
	var noconn = 0;

	console.log(card_bar_data_array)
//	for(var i = 0; i < card_bar_data_array.length; i++){
//		if(card_bar_data_array[i].get("id")==idx){
//			incycle = Number(card_bar_data_array[i].get("incycleTime"));
//			wait = Number(card_bar_data_array[i].get("waitTime"));
//			alarm = Number(card_bar_data_array[i].get("alarmTime"));
//			noconn = Number(card_bar_data_array[i].get("noconnTime"));
//		}
//	};
//		
//	var sum = incycle + wait + alarm + noconn;	
//	
//	chart.series[0].data[0].update(Number(Number(incycle/sum*100).toFixed(1)));
//	chart.series[0].data[1].update(Number(Number(wait/sum*100).toFixed(1)));
//	chart.series[0].data[2].update(Number(Number(alarm/sum*100).toFixed(1)));
	//chart.series[0].data[3].update(Number(Number(noconn/sum*100).toFixed(1)));
};

function drawStatusPie(id){
	$('#' + id).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            backgroundColor : "rgba(0,0,0,0)",
            marginTop : 0,
            marginBottom : 0,
            marginLeft : 0,
            marginRight : 0
        },
        title: {
            text: false
        },
        credits :false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
            	size : "100%",
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: 'black',
                        textShadow: false ,
                        fontSize : getElSize(35)
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: '가동',
                y: 5,
                color : "rgb(104,206,19)"
            }, {
                name: '비가동',
                y: 17,
                color : "rgb(109,109,109)"
            }], dataLabels: {
                distance: -getElSize(60)
            }
        }]
    });
};

function drawStatusPie2(id){
	$('#' + id).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            backgroundColor : "rgba(0,0,0,0)",
            marginTop : 0,
            marginBottom : 0,
            marginLeft : 0,
            marginRight : 0
        },
        title: {
            text: false
        },
        credits :false,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
            	size : "100%",
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '{point.y} ',
                    style: {
                        color: 'black',
                        textShadow: false ,
                        fontSize : getElSize(35)
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'In-Cycle',
                y: 3,
                color : "rgb(104,206,19)"
            }, {
                name: 'Wait',
                y: 1,
                color : "yellow"
            }, {
                name: 'Alarm',
                y: 1,
                color : "red"
            }, {
                name: 'No-Connection',
                y: 17,
                color : "rgb(109,109,109)"
            }], dataLabels: {
                distance: -getElSize(60)
            }
        }]
    });
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	

	$(".page").css({
		"height" : originHeight
	});


	$(".page").css({
		"top" : originHeight
	});
	
	$("#part1").css({
		"top" : 0,
		"overflow" : "hidden"
	});
	
	
	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(70)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});


	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1,
		"margin-left" : getElSize(150)
	});

	$(".title_right").css({
		"float" : "right",
		"width" : contentWidth * 0.1
	});

	$("#menu_btn").css({
		"width" : contentWidth * 0.04,
		"top" : marginHeight + (contentHeight * 0.01),
		"left" : getElSize(30),
		"z-index" : 5
	});

	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 999999,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(60)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});


	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	
	$(".wrap").css({
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		"margin" : getElSize(50)
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	var chart_height = getElSize(60);
	if(chart_height<20) chart_height = 20;
	
	$(".card_status").css({
		"height" : chart_height,
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$("#main_logo").css({
		"position" : "absolute",
		"width" : getElSize(600),
		//"top" : $("#menu_btn").offset().top + getElSize(30), 
		"top" : marginHeight + (contentHeight * 0.01) + getElSize(30),
		//"left" : $("#menu_btn").offset().left + $("#menu_btn").width() + getElSize(20),
		"left" : getElSize(30) + $("#menu_btn").width() + getElSize(20),
		"display" : "inline",
		"z-index" : 1
	});
	
	$("#time_table").css({
		"color" : "white",
		"position" : "absolute",
		"right" : marginWidth + getElSize(50),
		"top" : marginHeight + getElSize(20),
		"text-align" : "right",
		"font-size" : getElSize(40),
		"z-index" : 9
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.015
	});
	
	$(".menu_icon").css({
		"width" : getElSize(250),
		"background-color" : "white",
		"border-radius" : "50%",
	});

	$("p").css({
		"font-size" : getElSize(40),
		"font-weight" : "bolder"
	});
	
	$(".mainTable").not("#mainTable").css({
		"border-spacing" : getElSize(30)
	});
	
	$(".mainTable").css({
		"width" : "100%"
	});
	
	
	$(".td_header").css({
		"color" : "white",
		"font-size" : getElSize(50),
		"background-color" : "rgb(34,34,34)",
		"padding" : getElSize(20),
		"margin" : getElSize(10)
	});
	
	
	$("#incycleTime_avg").css({
		"color" : "white"
	});
	
	$(".upDown").css({
		"width" : getElSize(100),
		"margin-top" : getElSize(100)
	});
	
	$("#upFont").css({
		"color" : "rgb(124,224,76)",
		"font-size" : getElSize(100),
		"margin-left" : getElSize(50)
	});
	
	$("#downFont").css({
		"color" : "#FF3A3A",
		"font-size" : getElSize(100),
		"margin-left" : getElSize(50)
	});
	
	$("#thermometer, #thermometer2, #thermometer3").css({
		"width" : getElSize(80),
		"height" : getElSize(500),
		"margin-bottom" : getElSize(30),
		"margin-right" : getElSize(20),
		"margin-left" : getElSize(80),
		"border-radius" : getElSize(35),
		"margin- top" : getElSize(50),
		"float" : "left"
	});
	
	$("#thermometer .track, #thermometer2 .track, #thermometer3 .track").css({
		"width" : getElSize(30),
		"height" : getElSize(450),
		"top" : getElSize(25),
	});
	
	$("#thermometer .progress .amount, #thermometer2 .progress .amount, #thermometer3 .progress .amount").css({
		"padding": 0 + " " + getElSize(80) + " 0 " + getElSize(0),
		"font-size" : getElSize(30)
	});
	
	$("#diagram").css({
		"margin-top" : getElSize(50)
	});
	
	$("#reportDateDiv").css({
		"position" : "absolute",
		"right" : getElSize(50),
	});
	
	$("#sDate, #eDate").css({
//		"width" : getElSize(400),
//		"height" : getElSize(50),
		"font-size" : getElSize(40)
	});
	
	$("#map").css({
		"left" : (originWidth/2) - ($("#map").width()/2) - marginWidth 
	});
	
	$("#comName").css({
		"font-size" : getElSize(70)
	});
	
	$("#alarm").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(60),
		"margin-top" : getElSize(30),
		"margin-left" : getElSize(50)
	});
	
	$(".upDownSpan").css({
		"font-size" : getElSize(300)
	});
};

var csvData = "";
function csvSend(){
	csvData = '설비, 형번, 길 이(m)	, 설비가동시간 (시간), 현 비가동시간 (시간), 시간가동율 (%), 사이클타임 (분), 현재가동시간 (분), 완료사이클,생산량,대기상황,알람내역LINE'
	for(var i = 1; i <=23; i++){
		csvData+=(i + "호기") + ", , , , ,,,, , ,LINE";
	};
	
	var sDate, eDate;
	var csvOutput;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	csvOutput = csvData;
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

function drawStockChart() {
	var seriesOptions = [], seriesCounter = 0, names = [ 'MSFT', 'AAPL', 'GOOG' ],
	// create the chart when all data is loadedx
	createChart = function() {
		$('#container').highcharts('StockChart', {
			chart : {
				height : originHeight * 0.45
			},
			exporting : false,
			credits : false,
			rangeSelector : {
				selected : 4
			},

			rangeSelector : {
				buttons : [ {
					type : 'hour',
					count : 1,
					text : '1h'
				}, {
					type : 'day',
					count : 1,
					text : '1d'
				}, {
					type : 'month',
					count : 1,
					text : '1m'
				}, {
					type : 'year',
					count : 1,
					text : '1y'
				}, {
					type : 'all',
					text : 'All'
				} ],
				inputEnabled : true, // it supports only days
				selected : 4
			// all
			},

			yAxis : {
				labels : {
					formatter : function() {
						// return (this.value > 0 ? ' + ' : '') + this.value +
						// '%';
						return this.value
					}
				},
				plotLines : [ {
					value : 0,
					width : 2,
					color : 'silver'
				} ]
			},

			/*
			 * plotOptions: { series: { compare: 'percent' } },
			 */

			tooltip : {
				// pointFormat: '<span
				// style="color:{series.color}">{series.name}</span>:
				// <b>{point.y}</b> ({point.change}%)<br/>',
				valueDecimals : 2
			},

			series : seriesOptions
		});
	};

	seriesOptions[0] = {
		name : names[0],
		data : data_
	};

	/*
	 * $.each(names, function (i, name) {
	 * 
	 * $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=' +
	 * name.toLowerCase() + '-c.json&callback=?', function (data) {
	 * seriesOptions[i] = { name: name, data: data_ };
	 * 
	 * seriesCounter += 1;
	 * 
	 * if (seriesCounter === names.length) { createChart(); } }); });
	 */

	Highcharts.createElement('link', {
		href : '//fonts.googleapis.com/css?family=Unica+One',
		rel : 'stylesheet',
		type : 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	Highcharts.theme = {
		colors : [ "#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee",
				"#ff0066", "#eeaaee", "#55BF3B", "#DF5353", "#7798BF",
				"#aaeeee" ],
		chart : {
			backgroundColor : {
				linearGradient : {
					x1 : 0,
					y1 : 0,
					x2 : 1,
					y2 : 1
				},
				stops : [ [ 0, '#2a2a2b' ], [ 1, '#3e3e40' ] ]
			},
			style : {
				fontFamily : "'Unica One', sans-serif"
			},
			plotBorderColor : '#606063'
		},
		title : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase',
				fontSize : '20px'
			}
		},
		subtitle : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase'
			}
		},
		xAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			title : {
				style : {
					color : '#A0A0A3'

				}
			}
		},
		yAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			tickWidth : 1,
			title : {
				style : {
					color : '#A0A0A3'
				}
			}
		},
		tooltip : {
			backgroundColor : 'rgba(0, 0, 0, 0.85)',
			style : {
				color : '#F0F0F0'
			}
		},
		plotOptions : {
			series : {
				dataLabels : {
					color : '#B0B0B3'
				},
				marker : {
					lineColor : '#333'
				}
			},
			boxplot : {
				fillColor : '#505053'
			},
			candlestick : {
				lineColor : 'white'
			},
			errorbar : {
				color : 'white'
			}
		},
		legend : {
			itemStyle : {
				color : '#E0E0E3'
			},
			itemHoverStyle : {
				color : '#FFF'
			},
			itemHiddenStyle : {
				color : '#606063'
			}
		},
		credits : {
			style : {
				color : '#666'
			}
		},
		labels : {
			style : {
				color : '#707073'
			}
		},

		drilldown : {
			activeAxisLabelStyle : {
				color : '#F0F0F3'
			},
			activeDataLabelStyle : {
				color : '#F0F0F3'
			}
		},

		navigation : {
			buttonOptions : {
				symbolStroke : '#DDDDDD',
				theme : {
					fill : '#505053'
				}
			}
		},

		// scroll charts
		rangeSelector : {
			buttonTheme : {
				fill : '#505053',
				stroke : '#000000',
				style : {
					color : '#CCC'
				},
				states : {
					hover : {
						fill : '#707073',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					},
					select : {
						fill : '#000003',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					}
				}
			},
			inputBoxBorderColor : '#505053',
			inputStyle : {
				backgroundColor : '#333',
				color : 'silver'
			},
			labelStyle : {
				color : 'silver'
			}
		},

		navigator : {
			handles : {
				backgroundColor : '#666',
				borderColor : '#AAA'
			},
			outlineColor : '#CCC',
			maskFill : 'rgba(255,255,255,0.1)',
			series : {
				color : '#7798BF',
				lineColor : '#A6C7ED'
			},
			xAxis : {
				gridLineColor : '#505053'
			}
		},

		scrollbar : {
			barBackgroundColor : '#808083',
			barBorderColor : '#808083',
			buttonArrowColor : '#CCC',
			buttonBackgroundColor : '#606063',
			buttonBorderColor : '#606063',
			rifleColor : '#FFF',
			trackBackgroundColor : '#404043',
			trackBorderColor : '#404043'
		},

		// special colors for some of the
		legendBackgroundColor : 'rgba(0, 0, 0, 0.5)',
		background2 : '#505053',
		dataLabelsColor : '#B0B0B3',
		textColor : '#C0C0C0',
		contrastTextColor : '#F0F0F3',
		maskColor : 'rgba(255,255,255,0.3)'
	};

	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);
	createChart();
};