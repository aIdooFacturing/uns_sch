<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script type="text/javascript" src="${ctxPath }/js/moment.min.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/barChart.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/card.css">
</head>

<script>
	
	var momentStart=''
	var momentEnd=''
	
	function setTimeChart(){
		
		var startTime = $("#input-start-time").val();
		var endTime = $("#input-end-time").val();
		
		var splitStart = startTime.split('T')
		var splitEnd = endTime.split('T')
		
		if(momentStart=='' || momentEnd==''){
			momentStart = moment(splitStart[0]+' '+splitStart[1])
			momentEnd =moment(splitEnd[0]+' '+splitEnd[1])
		}else{
			
		}
		
		
		console.log(momentStart)
		console.log(momentEnd)
		
		var url =  ctxPath + "/mtc/setTimeChart.do";
		var param = "input=" + momentStart.format("YYYY-MM-DD HH:mm:ss") 
					
		$.ajax({
			url : url,
			data :param,
			dataType : "text",
			type : "post",
			success : function(data){
				console.log(data)
				if(data=='success'){
					
					momentStart.add(2,'m');
					
					if(momentStart.isSame(momentEnd)){
						
					}else{
						setTimeout(function(){
							setTimeChart();
						},500);
					}
				}else{
					
				}
			}
		});
		
		
	}

</script>

<style>
label{
	color:white;
}
</style>

<body oncontextmenu="setSlideMode(); return false">
	
	<label for="input-start-time" >시작 할 시간</label><input type="datetime-local" id="input-start-time">
	<label for="input-end-time" >종료 할 시간</label><input type="datetime-local" id="input-end-time">
	<button onclick="setTimeChart()">시작</button>
	
</body>
</html>