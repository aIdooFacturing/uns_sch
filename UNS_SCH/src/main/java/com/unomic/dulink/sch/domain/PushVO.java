package com.unomic.dulink.sch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PushVO {

	String dvcId;
	String startDateTime;
	String alarmMsg;
	String alarmCode;
	String name;
	String token;
	String mobile;
	String regTime;
}
