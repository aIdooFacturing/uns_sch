package com.unomic.dulink.sch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class ParamVo{
	String dvcId;
	String date;
	String dateTime;
	String stDate;
	String edDate;
	String tgDate;
	String sDate;
	String eDate;
	String adapterId;
	String lastUpdateTime;
	String lastStartDateTime;
	String lastChartStatus;
	Integer lastFeedOverride;
	Float lastSpdLoad;
	String lastAlarmCode;
	String lastAlarmMsg;
	String workDate;
	String lastProgramName;
	String lastProgramHeader;
	
	String lastCycleTime;
	
	String stopDate;
	String stopTime;
	
	//for night chart
	String targetDate;
	String nightStart;
	String nightEnd;
	String genStart;
	String genEnd;
	
	String ulkIp;
	String chartStatus;
	String startDateTime;
	
	String cycleCount;
	String cycleEndFiveMinutes;
	String cycleEndThreeMinutes;

	String avrCycleTime;
	String mcNo;
	
	Integer lastPartCount;
	String endDateTime;
	
}
