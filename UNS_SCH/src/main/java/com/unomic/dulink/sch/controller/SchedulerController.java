package com.unomic.dulink.sch.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.sch.domain.ParamVo;
import com.unomic.dulink.sch.service.SchService;
/**
 * Handles requests for the application home page.
 */
@Component
public class SchedulerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerController.class);
	
	private final String USER_AGENT = "Mozilla/5.0";
	
	@Autowired
	private SchService schService;
	
	//장비 요약 정보
	@Scheduled(fixedDelay = 60000)
	public void schAddDvcSmry(){
		if(CommonCode.isTest) return;
		String str, str2 = "";
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		
		if(cal.get(Calendar.HOUR_OF_DAY)<18) {
			cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH),18,0,0);
			cal2.set(cal2.get(Calendar.YEAR), cal2.get(Calendar.MONTH), cal2.get(Calendar.DATE),17,59,59);
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}else {
			cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH),18,0,0);
			cal2.set(cal2.get(Calendar.YEAR), cal2.get(Calendar.MONTH), cal2.get(Calendar.DATE),17,59,59);
			cal2.add(Calendar.DAY_OF_MONTH, +1);
		}
		
		cal.set(Calendar.MILLISECOND, 0);
		cal2.set(Calendar.MILLISECOND, 0);
		
		str = dayTime.format(cal.getTime());
		str2 = dayTime.format(cal2.getTime());
		
		ParamVo prmVo = new ParamVo();
		prmVo.setSDate(str);
		prmVo.setEDate(str2);
		prmVo.setWorkDate(str.substring(0, 10));
		
		prmVo.setDate(str.substring(0, 10));
		
		schService.addDvcSmry();
	/*	prmVo.setDate(CommonFunction.getYstDay());
		schService.addDvcSmry(prmVo);*/
		
		LOGGER.error("run schAddDvcSmry.");
	}
	
	//타임차트 입력.
	@Scheduled(fixedDelay = 30000)
	public void schAddTmCht(){
		if(CommonCode.isTest) return;
		ParamVo prmVo = new ParamVo();
		String dateTime = CommonFunction.getTodayDateTime();
		LOGGER.info("dateTime:"+dateTime);
		prmVo.setDateTime(CommonFunction.getTodayDateTime());
		try {
			schService.addDvcTmChart(prmVo);
		}catch(Exception e) {
			e.printStackTrace();
		}
		LOGGER.error("run schAddTmCht.");
	}
	
/*	//장비 마지막 상태 업데이트
	@Scheduled(fixedDelay = 2000)
	public void schEditDvcLast(){
		if(CommonCode.isTest) return;
		schService.editDvcLastStatus();
		schService.editDvcLast();
		ParamVo prmVo = new ParamVo();
		String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setDate(strDate);
		schService.setNoCon(prmVo);
		LOGGER.error("run schEditDvcLast.");
	}*/
	
	
	
	//초/분/시/ 00 한국시간보다 9시간빨라야함.
	@Scheduled(fixedRate=180000)	//	@Scheduled(fixedDelay=50000) : test
	public void schAddNightStatus(){
		LOGGER.error("run schAddNightStatus.");
		if(CommonCode.isTest) return;
		ParamVo prmVo = new ParamVo();
		String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setTgDate(strDate);
		schService.addNightStatus(prmVo);
		LOGGER.error("run schAddNightStatus.");
	}
	
	/*//초/분/시/ 00 한국시간보다 9시간빨라야함.
	@Scheduled(cron="30 30 6 * * ?")
	public void schAddNightStatus2(){
		LOGGER.error("run schAddNightStatus.");
		if(CommonCode.isTest) return;
		ParamVo prmVo = new ParamVo();
		String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setTgDate(strDate);
		schService.addNightStatus(prmVo);
		LOGGER.error("run schAddNightStatus.");
	}*/
	
	//초/분/시/ 00 한국시간보다 9시간빨라야함.
	@Scheduled(cron="0 0 9 * * ?")
	public void schBkOldAdtStatus(){
		LOGGER.error("start run schBkOldAdtStatus.");
		if(CommonCode.isTest) return;
		schService.bkOldAdtStatus();
		
		LOGGER.error("end run schBkOldAdtStatus.");
	}
	
	@Scheduled(cron="0 30 9 * * ?")
	public void schBkOldRaredata(){
		LOGGER.error("start run bkOldRaredata.");
		if(CommonCode.isTest) return;
		schService.bkOldRaredata();
		
		LOGGER.error("end run bkOldRaredata.");
	}
	
	@Scheduled(cron="0 0/30 * * * ?")
	public void schBkOldFrequentdata(){
		LOGGER.error("start run bkOldFrequentdata.");
		if(CommonCode.isTest) return;
		schService.bkOldFrequentdata();
		
		LOGGER.error("end run bkOldFrequentdata.");
	}
	
	@Scheduled(cron="0 0 10 * * ?")
	public void bkOldLampTimeChart(){
		LOGGER.error("start run bkOldLampTimeChart.");
		if(CommonCode.isTest) return;
		try {
			schService.bkOldLampTimeChart();
		} catch (Exception e) {
			// TODO 자동 생성된 catch 블록
			e.printStackTrace();
		}
		
		LOGGER.error("end run bkOldLampTimeChart.");
	}
	
	@Scheduled(cron="0 30 10 * * ?")
	public void bkOldAlarmList(){
		LOGGER.error("start run bkOldAlarmList.");
		if(CommonCode.isTest) return;
		try {
			schService.bkOldAlarmList();
		} catch (Exception e) {
			// TODO 자동 생성된 catch 블록
			e.printStackTrace();
		}
		
		LOGGER.error("end run bkOldAlarmList.");
	}
	
	@Scheduled(cron="0 00 11 * * ?")
	public void bkOldStatusList(){
		LOGGER.error("start run bkOldStatusList.");
		if(CommonCode.isTest) return;
		try {
			schService.bkOldStatusList();
		} catch (Exception e) {
			// TODO 자동 생성된 catch 블록
			e.printStackTrace();
		}
		
		LOGGER.error("end run bkOldStatusList.");
	}
	
	@Scheduled(cron="0 30 11 * * ?")
	public void bkOldLampdata(){
		LOGGER.error("start run bkOldLampdata.");
		if(CommonCode.isTest) return;
		try {
			schService.bkOldLampdata();
		} catch (Exception e) {
			// TODO 자동 생성된 catch 블록
			e.printStackTrace();
		}
		
		LOGGER.error("end run bkOldLampdata.");
	}
	
	
	
	/*//?tgDate=yyyy-mm-dd
	@RequestMapping(value="runAddNightStatus")
	@ResponseBody
	public String runAddNightStatus(String tgDate){
		ParamVo prmVo = new ParamVo();
		//String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setTgDate(tgDate);
		schService.addNightStatus(prmVo);
		LOGGER.error("run addNightStatus.");
		return "OK";
	}
		
	@RequestMapping(value="runEditDvcLast")
	@ResponseBody
	public String runEditDvcLast(String tgDate){
		schService.editDvcLastStatus();
		schService.editDvcLast();
		ParamVo prmVo = new ParamVo();
		//String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setDate(tgDate);
		schService.setNoCon(prmVo);
		LOGGER.error("run schEditDvcLast.");
		return "OK";
	}
	
	

	@RequestMapping(value="noConTest")
	@ResponseBody
	public String noConTest(){
		ParamVo prmVo = new ParamVo();
		
		String strDate = CommonFunction.mil2WorkDate(System.currentTimeMillis());
		prmVo.setDate(strDate);
		schService.setNoCon(prmVo);
		return "OK:"+strDate;
	}
	
	@RequestMapping(value="spTest")
	@ResponseBody
	public void spTest(){
		//DeviceVo dvcVo = new DeviceVo();
		//dvcVo.setWorkDate("2015-10-06");
		//deviceService.calcDeviceTimesTest(dvcVo);
		//deviceService.calcDeviceTimes(dvcVo);
		//calcDeviceTimes();
	}
	
	//?date=2017-06-23
	@RequestMapping(value="addDvcSmryTest")
	@ResponseBody
	public String addDvcSmryTest(ParamVo prmVo){
		//prmVo.setDate("2017-06-23");
		schService.addDvcSmry(prmVo);
		
		return "OK";
	}
	
	@RequestMapping(value="addDvcLastTest")
	@ResponseBody
	public String addDvcLastTest(){
		schService.addDvcLast();
		return "OK";
	}
	
	
	@RequestMapping(value="editDvcLastTest")
	@ResponseBody
	public String editDvcLastTest(){
		schService.editDvcLast();
		return "OK";
	}
	
	@RequestMapping(value="editDvcLastStatusTest")
	@ResponseBody
	public String editDvcLastStatusTest(){
		schService.editDvcLastStatus();
		return "OK";
	}
	
	@RequestMapping(value="addTmChtTest")
	@ResponseBody
	public String addTmChtTest(){
		
		ParamVo prmVo = new ParamVo();
		
		String dateTime = CommonFunction.getTodayDateTime();
		LOGGER.info("dateTime:"+dateTime);
		prmVo.setDateTime(CommonFunction.getTodayDateTime());
		
		schService.addDvcTmChart(prmVo);
		LOGGER.error("run schAddTmCht.");
		
		return "OK:"+CommonFunction.getTodayDateTime();
		
	}*/
	
	/*@RequestMapping(value="batchAddDvcSmry")
	@ResponseBody
	public String batchTimeChart(ParamVo inputVo){
		LOGGER.info("stDate:"+inputVo.getStDate());
		LOGGER.info("edDate:"+inputVo.getEdDate());
		if(inputVo.getStDate()==null || inputVo.getEdDate()==null){
			return "fail-check param";
		}
		LOGGER.info("inputVo.getStDate():"+inputVo.getStDate());
		LOGGER.info("inputVo.getEdDate():"+inputVo.getEdDate());
		String edDate = inputVo.getEdDate();
			ParamVo prmVo = new ParamVo();
	       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	       //DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.zzz");
	       dateFormat.setTimeZone(TimeZone.getTimeZone(CommonCode.TIME_ZONE));
           Date dateSt = null;
           try {
                  dateSt = dateFormat.parse(inputVo.getStDate());
           } catch (ParseException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }
           
           Calendar cal = Calendar.getInstance();

           cal.setTime(dateSt);
           String tgDate = dateFormat.format(cal.getTime());
           LOGGER.info("tgDate:"+tgDate);

           inputVo.setTgDate(tgDate);
           
           LOGGER.info("getTgDate:"+inputVo.getTgDate());
           LOGGER.info("edDate:"+edDate);
           

           //CommonFunction.dateTime2Mil(tgDate)
           //while(!tgDate.equals(edDate)){
           //while(CommonFunction.dateTime2Mil(tgDate) < (CommonFunction.dateTime2Mil(edDate))){
           while(!tgDate.equals(edDate)){
        	   LOGGER.info("@@@@@@@@@@@@@@@@@@ING@@@@@@@@@@@@@@@@@@");
        	   //deviceService.addTimeChart(inputVo);
        	   prmVo.setDate(inputVo.getTgDate());
        	   
        	   schService.addDvcSmry(prmVo);

        	   cal.add(Calendar.DATE, 1);
               tgDate = dateFormat.format(cal.getTime());
               inputVo.setTgDate(tgDate);
               LOGGER.info("tgDate:"+tgDate);
           }
           // EOL
           LOGGER.info("@@@@@@@@@@@@@@@@@@EOL@@@@@@@@@@@@@@@@@@");
           LOGGER.info("edDate:"+tgDate);
           LOGGER.info("edDate:"+edDate);
 
		return "OK";
	}*/
	
	/**
	 * Sets the start date.
	 * @since 2018-02-20 15:30
	 * @author John.Joe
	 * 매시 시 0,30 분 마다 날짜 변경 시간을 체크하고
	 * 해당 시간시에 날짜 변경점을 기록함
	 */
	@Scheduled(cron = "0 0/30 * * * *")
	public void setStartDate(){
		if(CommonCode.isTest) return;
		try {
			schService.setStartDate();
		} catch (Exception e) {
			// TODO 자동 생성된 catch 블록
			e.printStackTrace();
		}
	}
	
	/*@Scheduled(cron = "0 0 18 * * *")*/
	public void setAlarmStartDate(){
		if(CommonCode.isTest) return;
		try {
			schService.setAlarmStartDate();
		} catch (Exception e) {
			// TODO 자동 생성된 catch 블록
			e.printStackTrace();
		}
	}
	
	/**
	 * @author John.Joe 
	 * @since 2018-02-27 
	 * 
	 * NO-CONNECTION 발생 감지
	 * 
	 * 
	 */
	@Scheduled(fixedDelay = 60000)
	public void checkConnection(){
		if(CommonCode.isTest) return;
		try {
			schService.checkConnection();
		}catch (Exception e) {
			
		}
		
	}
	
	/**
	 * @author John.Joe 
	 * @since 2018-07-09 
	 * 
	 * 지난 1분간의 알람 발생 감지
	 * 알람이 있으면 push
	 * 
	 */
	@Scheduled(fixedRate = 60000)
	public void sendAlarmPush(){
		if(CommonCode.isTest) return;
		try {
			schService.sendAlarmPush();
		}catch (Exception e) {
			
		}
	}
	
	// kakaotalk alarm push
	@Scheduled(fixedRate = 60000)
	public void sendKakaoPush(){
		if(CommonCode.isTest) return;
		try {
			schService.sendKakaoPush();
		}catch (Exception e) {
			
		}
	}
	
	@Scheduled(fixedRate = 8000)
	public void setChartStatus(){
		if(CommonCode.isTest) return;
		try {
			
			schService.setChartStatus();
			}catch (Exception e) {
			
		}
	}
	
	@Scheduled(fixedRate = 5000)
	public void updateDeviceLast(){
		if(CommonCode.isTest) return;
		try {
			schService.updateDeviceLast();
		}catch (Exception e) {
			
		}
	}
	
	@Scheduled(fixedRate = 10000)
	public void setProduction(){
		if(CommonCode.isTest) return;
		try {
			schService.setProduction();
		}catch (Exception e) {
			
		}
	}
	
	
	@Scheduled(fixedDelay = 10000)
	public void getLampStatus(){
		String result = "";
		BufferedReader in = null;
		if(CommonCode.isTest) return;
		try {
			result = schService.getLampStatus();
			
			URL obj = new URL("http://mtmes.doosanmachinetools.com/DULink/chart/setLampStatus.do"); // 호출할 url
            HttpURLConnection con = (HttpURLConnection)obj.openConnection();
            
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");
            
            OutputStream os = con.getOutputStream();
            os.write(result.getBytes("UTF-8"));
            os.flush();
            os.close();
 
            in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	
}

