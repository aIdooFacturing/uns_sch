package com.unomic.dulink.sch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StatusVO {
	String dvcId;
	String chart;
	String start;
	String endTime;
	String regtime;
	float spdLoad;
	int spdOverride;
	float spdLoadZ;
	
	int spdAct;
	String prgmName;
	String pmcEmg;
	String pmcCut;
	int pmcFeedHold;
	int pmcSingleBlock;
	int pmcStl;
	String status;
	String mode;
	int m1;
	int m2;
	int m3;
}
