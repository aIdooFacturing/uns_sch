package com.unomic.dulink.sch.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
//import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.sch.domain.LampVO;
import com.unomic.dulink.sch.domain.ParamVo;
import com.unomic.dulink.sch.domain.PushVO;
import com.unomic.dulink.sch.domain.StatusVO;

import jxl.write.DateTime;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

@Service
@Repository
public class SchServiceImpl implements SchService {

	private final static String SCH_SPACE = "com.unos.sch.";

	private final static Logger LOGGER = LoggerFactory.getLogger(SchServiceImpl.class);

	@Autowired
	@Resource(name = "sqlSession_ma")
	private SqlSession sql_ma;

	@Override
	@Transactional // (value="txManager_ma")
	public String addDvcSmry() {
		LOGGER.info("RunAddDvcSmry");
		try {
			sql_ma.insert(SCH_SPACE + "addDvcSmry");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "OK";
	}

	@Override
	@Transactional // (value="txManager_ma")
	public String addDvcTmChart(ParamVo prmVo) {
		try {
			sql_ma.insert(SCH_SPACE + "addDvcTmChart", prmVo);
			sql_ma.insert(SCH_SPACE + "addLampTmChart", prmVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "OK";
	}

	public String addDvcLast() {
		sql_ma.insert(SCH_SPACE + "addDvcLast");
		return "OK";
	}

	public String setNoCon(ParamVo prmVo) {
		sql_ma.insert(SCH_SPACE + "setNoCon", prmVo);
		return "OK";
	}

	@Override
	@Transactional
	public String addNightStatus(ParamVo prmVo) {
		try {
			sql_ma.insert(SCH_SPACE + "addNightStatus", prmVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "OK";
	}

	public String editDvcLast() {
		LOGGER.info("RUN editDvcLast");
		sql_ma.update(SCH_SPACE + "editDvcLast");
		return "OK";
	}

	public String editDvcLastStatus() {
		LOGGER.info("RUN editDvcLastStatus");
		sql_ma.update(SCH_SPACE + "editDvcLastStatus");
		return "OK";
	}

	public String bkOldAdtStatus() {
		LOGGER.info("RUN bkOldAdtStatus");
		sql_ma.insert(SCH_SPACE + "bkOldAdtStatus");
		return "OK";
	}

	public String bkOldRaredata() {
		LOGGER.info("RUN bkOldRaredata");
		sql_ma.insert(SCH_SPACE + "bkOldRaredata");
		return "OK";
	}

	public String bkOldFrequentdata() {
		LOGGER.info("RUN bkOldFrequentdata");
		sql_ma.insert(SCH_SPACE + "bkOldFrequentdata");
		return "OK";
	}

	@Override
	public void setStatus() {
		sql_ma.insert(SCH_SPACE + "setStatus");

	}

	@Override
	public void setAlarmStartDate() throws Exception {
		// TODO 占쎌쁽占쎈짗 占쎄문占쎄쉐占쎈쭆 筌롫뗄�꺖占쎈굡 占쎈뮞占쎈��
		try {
			sql_ma.update(SCH_SPACE + "updateAlarmEndTime");
			sql_ma.insert(SCH_SPACE + "insertAlarmStartDate");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @since 2018-02-25 15:30
	 * @author John.Joe
	 * 
	 *         18占쎈뻻 占쎌뵠占쎌뜎占쎈퓠 占쎌뵠占쎌읈 占쎈쑓占쎌뵠占쎄숲占쎈퓠 占쏙옙占쎈릭占쎈연 END_DATE_TIME 占쎌뱽 疫꿸퀡以됵옙釉��⑨옙 占쎈쑓占쎌뵠占쎄숲 占쎌쟿�굜遺얜굡�몴占� 癰귣벊沅쀯옙釉�占쎈연 占쎄퉱嚥≪뮇�뵠
	 *         START_DATE_TIME 占쎌뱽 筌욑옙占쎌젟 占쎄텊筌욎뮄占� 癰귨옙野껋럥由븝옙肉�占쎌벉占쎈퓠占쎈즲 �겫�뜃�럡占쎈릭�⑨옙 占쎌뵠占쎌읈占쎄텊嚥≪뮆占쏙옙苑� 占쎌뵠占쎈선筌욑옙 占쎌삢�뜮占� 占쎄맒占쎄묶占쎈퓠 占쏙옙占쎈릭占쎈연 占쎈뻻揶쏉옙 疫꿸퀡以됵옙�뵠
	 *         �겫�뜃占쏙옙�뮟 占쎈뻥占쎈쐲占쎌젎占쎌뱽 揶쏆뮇苑묕옙釉�疫뀐옙 占쎌맄占쎈립 �굜遺얜굡
	 * 
	 */
	@Override
	public void setStartDate() throws Exception {
		try {
			sql_ma.insert(SCH_SPACE + "setStartData");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author John.Joe
	 * @since 2018-02-26 16:19
	 * 
	 *        占쎈쑓占쎌뵠占쎄숲 占쎈씜占쎈쑓占쎌뵠占쎈뱜占쎈뻻揶쏄쑴�뱽 占쎈솇占쎈뼊占쎈릭占쎈연 5�겫袁㏃퍢 占쎌뿯占쎌젾占쎌뵠 占쎈씨占쎌뱽 野껋럩�뒭 No-Connection 筌ｌ꼶�봺
	 * 
	 */
	@Override
	public void checkConnection() throws Exception {
		// TODO 占쎌쁽占쎈짗 占쎄문占쎄쉐占쎈쭆 筌롫뗄�꺖占쎈굡 占쎈뮞占쎈��
		try {
			List<ParamVo> deviceVoList = sql_ma.selectList(SCH_SPACE + "checkConnection");

			List<ParamVo> addVoList = new ArrayList<ParamVo>();
			if (deviceVoList.size() > 0) {
				for (int i = 0; i < deviceVoList.size(); i++) {
					if (!deviceVoList.get(i).getChartStatus().equals("NO-CONNECTION")) {
						addVoList.add(deviceVoList.get(i));
					}
				}
				sql_ma.update(SCH_SPACE + "updateNoconnection", addVoList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getLampStatus() throws Exception {
		List<LampVO> dataList = sql_ma.selectList(SCH_SPACE + "getLampStatus");

		List list = new ArrayList();

		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();

			map.put("Red", dataList.get(i).getRed());
			map.put("Yellow", dataList.get(i).getYellow());
			map.put("Green", dataList.get(i).getGreen());
			map.put("RedBlink", dataList.get(i).getRedBlink());
			map.put("YellowBlink", dataList.get(i).getYellowBlink());
			map.put("GreenBlink", dataList.get(i).getGreenBlink());
			map.put("addressValue", dataList.get(i).getAddressValue());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("UPTIME", dataList.get(i).getUPTIME());

			list.add(map);
		}
		;

		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();

		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;

	}

	@Override
	public void setChartStatus() throws Exception {
		try {
			long start = System.currentTimeMillis(); //占쎈뻻占쎌삂占쎈릭占쎈뮉 占쎈뻻占쎌젎 �④쑴沅�
			
			List<StatusVO> list = sql_ma.selectList(SCH_SPACE + "setChartStatus");
			List<StatusVO> beforeList = sql_ma.selectList(SCH_SPACE + "getLastChartStatus");
		
			long end = System.currentTimeMillis(); //占쎈늄嚥≪뮄�젃占쎌삪占쎌뵠 占쎄국占쎄돌占쎈뮉 占쎈뻻占쎌젎 �④쑴沅�
			
			
			long start2 = System.currentTimeMillis(); //占쎈뻻占쎌삂占쎈릭占쎈뮉 占쎈뻻占쎌젎 �④쑴沅�
			List<StatusVO> newList = new ArrayList<StatusVO>();
			List<StatusVO> newList2 = new ArrayList<StatusVO>();
			Date date = new Date();
			String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

			String dvcId = "0";

			int size = list.size();
			int size2 = beforeList.size();
			
			
			int checked=0;
			for(int i=0;i<size2;i++) {
				
				checked=0;
				
				for(int j=0;j<size;j++) {
					
					if(checked==0) {
						if(beforeList.get(i).getDvcId().equals(list.get(j).getDvcId())) { //dvcId癰귨옙 筌ㅼ뮄�젏 占쎄맒占쎄묶占쏙옙 �뜮袁㏉꺍占쎈릭占쎈연 dvcId揶쏉옙 揶쏆늽��
							if(beforeList.get(i).getChart().equals(list.get(j).getChart())) { // 占쎄맒占쎄묶揶쏉옙 揶쏆늿�몵筌롳옙 占쎈솭占쎈뮞
								
							}else { // dvcId揶쏉옙 揶쏆늽�� 占쎄맒占쎄묶揶쏉옙 占쎈뼄�몴���늺 占쏙옙占쎌삢
								
								Date beforeDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(beforeList.get(i).getStart());
								Date listDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(list.get(j).getStart());
								int compare=beforeDate.compareTo(listDate);
								if(compare<0) {
									if(newList.contains(list.get(j))) {
										
									}else {
										
										list.get(j).setEndTime(null);
										list.get(j).setRegtime(format);
										newList.add(list.get(j));
										
									}
									checked=1;
								}
							}
						}
					}else {
					}
				}
			}
			
			for (int i = size - 1; i >= 0; i--) {
				for (int j = 0; j < newList.size(); j++) {
					if (list.get(i).getDvcId().equals(newList.get(j).getDvcId())) {
						
						Date rawDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(list.get(i).getStart());
						Date newListDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(newList.get(j).getStart());
						
						int compare = newListDate.compareTo(rawDate);
						if(compare>=0) {
							list.remove(i);
						}else {
						}
						
						
					}
				}
			}
		
			String dvcId2="0";
			int count3=0;
			int exist=0;
			
			for(int i=0;i<list.size();i++) {
				
				if(!dvcId2.equals(list.get(i).getDvcId())) {
					for(int j=0;j<newList.size();j++) {
						if(newList.get(j).getDvcId().equals(list.get(i).getDvcId())) {
							exist++;
							if(!newList.get(j).getChart().equals(list.get(i).getChart())) {
								list.get(i).setEndTime(null);
								
								list.get(i).setRegtime(format);
								newList2.add(list.get(i));
								dvcId2=list.get(i).getDvcId();
								count3=1;
							}
						}
					}
					
					if(count3==0 && exist==0) {
						for(int j=0;j<beforeList.size();j++) {
							if(beforeList.get(j).getDvcId().equals(list.get(i).getDvcId())){
								if(!beforeList.get(j).getChart().equals(list.get(i).getChart())) {
									
									list.get(i).setRegtime(format);
									newList2.add(list.get(i));
									list.get(i).setEndTime(null);
									dvcId2=list.get(i).getDvcId();
									count3=0;
									exist=0;
								}
							}
						}
					}
				}else {
					if(list.get(i).getDvcId().equals(list.get(i-1).getDvcId())){
						if(!list.get(i).getChart().equals(list.get(i).getChart())) {
							
							list.get(i).setRegtime(format);
							newList2.get(newList2.size()-1).setEndTime(list.get(i).getStart());
							newList2.add(list.get(i));
						}
					}
					
				}
			}
			
		
			if (newList.size() > 0) {
				sql_ma.insert(SCH_SPACE + "updateLastStatus", newList);
			}

			if(newList2.size()>0) {
				sql_ma.insert(SCH_SPACE + "insertStatus", newList2);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateDeviceLast() throws Exception {
		try {
			sql_ma.update(SCH_SPACE + "updateDeviceLast");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setProduction() throws Exception {
		// TODO 占쎌쁽占쎈짗 占쎄문占쎄쉐占쎈쭆 筌롫뗄�꺖占쎈굡 占쎈뮞占쎈��
		try {
			sql_ma.update(SCH_SPACE + "setProduction");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//wilson
	public void sendKakaoPush() throws Exception {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar c1 = Calendar.getInstance();
        String strToday = sdf.format(c1.getTime());
        int msgidNum = 0;
        String type = "AT";

        //System.out.println("Today=" + strToday);
        
		try {
			List<PushVO> list = sql_ma.selectList(SCH_SPACE + "sendAlarmKakao");
			
			if(list.size() > 0) {
				OkHttpClient client = new OkHttpClient();
		        MediaType mediaType = MediaType.parse("text/plain");
		        
		        String strBody = "[";
		        
	        	for(PushVO push : list) {
	        		
		        	strBody += ",{"
			                + "\"msgid\":\"" + strToday + Integer.toString(msgidNum) + "\","
			                + "\"message_type\":\"" + type + "\","
			                + "\"profile_key\":\"392b51d35bd98a5693bb7d739882b254a5ab60fa\","
			                + "\"receiver_num\":\"" + push.getMobile() + "\","
			                + "\"template_code\":\"idoo_001\","
							+ "\"message\":\"[iDOO] 장비 알람 발생\n□ 장비명 : " + push.getName() + "\n□ 알람메시지 :\n  " + push.getAlarmMsg() + "\n□ 알람시간 :\n  " + push.getStartDateTime() + "\","
							+ "\"reserved_time\": \"00000000000000\""
			                + "}";
		        	
//			        	System.out.println("Today=" + strToday + "_" + Integer.toString(msgidNum));
//			        	System.out.println("mobile= " + push.getMobile());
		        	
		        	msgidNum += 1;	        	
		        	
		        	//wison (to check alarm is send) 									
					PushVO pushTest = new PushVO();
					pushTest.setName(push.getName());
					pushTest.setAlarmCode(push.getAlarmCode());
					pushTest.setAlarmMsg(push.getAlarmMsg());
					pushTest.setStartDateTime(push.getStartDateTime());
					pushTest.setMobile(push.getMobile());
					pushTest.setRegTime(push.getRegTime());
					pushTest.setDvcId(push.getDvcId());
					
					sql_ma.insert(SCH_SPACE + "checkAlarm", pushTest);
		        }
		        
		        strBody += "]";

		        String jsonArray = new String(strBody);
		        
		        RequestBody body = RequestBody.create(mediaType, jsonArray);
		        Request req = new Request.Builder()
		                .url("https://alimtalk-api.sweettracker.net/v2/392b51d35bd98a5693bb7d739882b254a5ab60fa/sendMessage")
		                .post(body)
		                .addHeader("userid", "DMT1808")
		                .addHeader("content-type", "application/json;charset=UTF-8")
		                .build();
		        Response res = client.newCall(req).execute();
		        ResponseBody responseBody = res.body();
		        String str = responseBody.string();
//		        System.out.println(str);
			}
			
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	

	/**
 	 * @author wilson
 	 * @since 2019-12-26 占쎈르占쎌뿺 占쎈툡雅뚳옙 占쎌뵬�겫占쏙쭕占� 癰귣�沅∽쭪占쏙옙�뮉 �눧紐꾩젫 占쎈땾占쎌젟
 	 * (inner => left outer join, left join => inner join) 
 	 * (token占쎈퓠 null占쎌뵠 占쎈굶占쎈선揶쏉옙筌롳옙 癰귣�沅∽쭪占쏙쭪占� 占쎈륫占쎈뮉 �눧紐꾩젫占쎌젎)
 	 */
	
	@Override
	public void sendAlarmPush() throws Exception {
		// TODO 占쎌쁽占쎈짗 占쎄문占쎄쉐占쎈쭆 筌롫뗄�꺖占쎈굡 占쎈뮞占쎈��
		try {
			List<PushVO> list = sql_ma.selectList(SCH_SPACE + "sendAlarmPush");
			
			//System.out.println(list.size()+"揶쏆뮇�벥 占쎈쳳占쎈뻻 占쎈르占쎌뿺 �뵳�딅뮞占쎈뱜 獄쏆뮇源�");
			//LOGGER.info("�뵳�딅뮞占쎈뱜 占쎄텢占쎌뵠筌앾옙 : " + list.size());
			
			//int index = 0;
			
			for(PushVO push : list) {
				
				//LOGGER.info(index + ")" + push.getToken() + " | " + push.getName() + " | " + push.getAlarmMsg() + " | " + push.getStartDateTime() + "\n");
				//index ++;

				sendPush(push.getToken(), push.getName(), push.getAlarmMsg(), push.getStartDateTime());
				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	};
	
	
	
	public void sendPush(String token, String id, String body, String sDate){
		JSONObject obj = new JSONObject();	
		obj.put("to", token);
		//foreground
		JSONObject dataObj = new JSONObject();
		dataObj.put("title", id);
		dataObj.put("body", body+"\n\n"+sDate);
		dataObj.put("sound", "default");

		/*
		//background
		JSONObject notiObj = new JSONObject();
		notiObj.put("sound", "default");
		notiObj.put("body", body);
		notiObj.put("title", title);
		
		obj.put("notification", notiObj);
		*/
		obj.put("notification", dataObj);
		//System.out.println(obj);
		//LOGGER.info("(obj)\n" + obj);
		
		try {
			URL u = new URL("https://fcm.googleapis.com/fcm/send");
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty( "Content-Type", "application/json" );
			conn.setRequestProperty( "Authorization", "key=AIzaSyDAl5OMbHU3Nf-35_tCoPl8lHXzClEeIgc");
			OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
			osw.write(obj.toString());
			osw.flush();

			BufferedReader br = null;
			br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

			osw.close();
			br.close();
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void bkOldLampTimeChart() throws Exception {
		sql_ma.insert(SCH_SPACE + "bkOldLampTimeChart");
	}

	@Override
	public void bkOldAlarmList() throws Exception {
		sql_ma.insert(SCH_SPACE + "bkOldAlarmList");
	}

	@Override
	public void bkOldStatusList() throws Exception {
		// TODO 占쎌쁽占쎈짗 占쎄문占쎄쉐占쎈쭆 筌롫뗄�꺖占쎈굡 占쎈뮞占쎈��
		
		sql_ma.insert(SCH_SPACE + "bkOldStatusList");
		
	}

	@Override
	public void bkOldLampdata() throws Exception {
		// TODO 占쎌쁽占쎈짗 占쎄문占쎄쉐占쎈쭆 筌롫뗄�꺖占쎈굡 占쎈뮞占쎈��
		sql_ma.insert(SCH_SPACE + "bkOldLampdata");
	}
	

}