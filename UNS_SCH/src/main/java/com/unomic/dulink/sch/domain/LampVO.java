package com.unomic.dulink.sch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LampVO {
	
	 private String userid;
		private int count;
		private int red;
		private int redBlink;
		private int yellow;
		private int yellowBlink;
		private int green;
		private int greenBlink;
		private int dvcId;
		private String address;
		private String addressValue;
		private String name;
		
	    String redExpression;
	    String redBlinkExpression;
	    String yellowExpression;
	    String yellowBlinkExpression;
	    String greenExpression;
	    String greenBlinkExpression;
	    
	    String UPTIME;
	    
	    String startDateTime;
	    String endDateTime;
	    
	    String chartStatus;
	    
}
