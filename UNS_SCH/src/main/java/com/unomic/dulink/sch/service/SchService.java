package com.unomic.dulink.sch.service;

import com.unomic.dulink.sch.domain.ParamVo;

public interface SchService {
	public String addDvcSmry();
	public String addDvcLast();
	public String editDvcLast();
	public String editDvcLastStatus();
	public String addDvcTmChart(ParamVo prmVo);
	public String setNoCon(ParamVo prmVo);
	public String addNightStatus(ParamVo prmVo);
	public String bkOldAdtStatus();
	public String bkOldRaredata();
	public String bkOldFrequentdata();
	public void setStatus();
	public void setAlarmStartDate() throws Exception;
	public void setStartDate() throws Exception;
	public void checkConnection() throws Exception;
	public String getLampStatus() throws Exception;
	public void setChartStatus() throws Exception;
	public void updateDeviceLast() throws Exception;
	public void setProduction() throws Exception;
	public void sendAlarmPush() throws Exception;
	public void sendKakaoPush() throws Exception;
	
	public void bkOldLampTimeChart() throws Exception;
	public void bkOldAlarmList() throws Exception;
	public void bkOldStatusList() throws Exception;
	public void bkOldLampdata() throws Exception; 
}
