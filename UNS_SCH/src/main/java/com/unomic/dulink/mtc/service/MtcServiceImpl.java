package com.unomic.dulink.mtc.service;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.mtc.domain.MtcCtrlVo;

@Service
@Repository
public class MtcServiceImpl implements MtcService{

	private final static String DEVICE_SPACE= "com.unos.sch.";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MtcServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;

	@Override
	public String setTimeChart(String input) {
		String result = "failed";
		try {
			sql_ma.insert(DEVICE_SPACE+"setTimeChart", input);
			sql_ma.insert(DEVICE_SPACE+"addLampTmChart2", input);
			result="success";
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void setStartDate() throws Exception {
		
		long start = System.currentTimeMillis(); //시작하는 시점 계산
		 
		try {
			sql_ma.update(DEVICE_SPACE + "updateEndTimeStatus");
			sql_ma.insert(DEVICE_SPACE + "insertStartStatus");
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		long end = System.currentTimeMillis(); //프로그램이 끝나는 시점 계산
		System.out.println( "실행 시간 : " + ( end - start )/1000.0 +"초"); //실행 시간 계산 및 출력

	}
	
//	@Override
//	@Transactional(value="txManager_ma")
//	public String setChgDvcCtrl(MtcCtrlVo inputVo)
//	{
//		
//		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE + "cntCtrlChg", inputVo);
//		if(cntDvc > 0){
//			sql_ma.update(DEVICE_SPACE + "editCtrlChg", inputVo);	
//		}else{
//			sql_ma.insert(DEVICE_SPACE + "addCtrlChg", inputVo);
//		}
//
//		return "OK";
//	}

}
