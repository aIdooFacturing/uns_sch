//170621 Work start

package com.unomic.dulink.mtc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.mtc.service.MtcService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/mtc")
@Controller
public class MtcController {
	
	private static final Logger logger = LoggerFactory.getLogger(MtcController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
		
	@Autowired
	private MtcService mtcService;
	
	@RequestMapping(value = "getInitTime")
	@ResponseBody
    public String setInitTime(){
		return CommonFunction.getTodayDateTime();
	}
	
	@RequestMapping(value = "timeTest")
	@ResponseBody
    public String timeTest(){
		return CommonFunction.getTodayDateTime();
	}
	
	@RequestMapping(value = "barChart2")
	public String barChart2(){
		return "chart/barChart2";
	}
	
	@RequestMapping(value = "setTimeChart")
	@ResponseBody
	public String setTimeChart(String input){
		String result = "failed";
		try{
			result = mtcService.setTimeChart(input);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "setStartDate")
	public void setStartDate(){
		try {
			System.out.println("setStartDate");
			mtcService.setStartDate();
		} catch (Exception e) {
			// TODO 자동 생성된 catch 블록
			e.printStackTrace();
		}
	}
	
}
